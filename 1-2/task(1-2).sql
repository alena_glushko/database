Use ABank
Go
/*��������� ���� Number � NOT NULL �� NULL (��� ���������� ���������� �������)*/
ALTER Table Client ALTER COLUMN Number char(12) NULL;
/*�1 (�)*/
INSERT INTO Client VALUES ('Vasya','Vasek','vas@gmail.com','KO121314','380987654321'),('Katya','Katko','kat@gmail.com','KO151617','380987654389'),('Vasya','Vlasov','vlasov@gmail.com','KO181920','380981111111'), ('Alex','Alexov','alex@gmail.com','KO181925','380982222222'),('Alex','Vlasov','alex_vlasov@gmail.com','KO181912','380983333333');
/*�1 (b)*/
INSERT INTO Client (Name, Surname, Email, PassportCode) VALUES ('Vasya','Vasechka','vasechka@gmail.com','KO989978');
/*�1 (c)*/
SELECT ClientId, Name, Surname INTO ClientVasya FROM Client WHERE Name='Vasya';
/*�2 (�)*/
UPDATE Client SET Number=380999999999 WHERE Surname='Alexov';
/*���������� ���������� ������ (��� ���������� ������)*/
INSERT INTO Currency VALUES ('eur'),('$');
INSERT INTO DepositType VALUES ('type1', '1','2000', '5 mounth'),('type2', '2','200', '6 mounth');
INSERT INTO Deposit VALUES ('1', '2018.01.5','2018.06.5', '1000', '1'),('2', '2018.03.5','2018.08.5', '1500', '1');
INSERT INTO CreditType VALUES ('Credit1', '1','2000', '1.8'),('Credit2', '1','1500', '1.5');
INSERT INTO Credit VALUES ('1', '2018.01.05','2018.05.05', '500','2'),('2', '2018.01.06','2018.05.06', '700','2'), ('2', '2018.01.06','2018.05.06', '700','3');
/*�2 (b)*/
UPDATE CreditType SET MaxSum=MaxSum+500 WHERE MaxSum>1600;
/*�2 (c)*/
UPDATE DepositType  
SET DepositType.CurrencyId = 2
FROM DepositType
INNER JOIN Currency
ON DepositType.CurrencyId=Currency.CurrencyId
WHERE DepositType.MinSum='2000';